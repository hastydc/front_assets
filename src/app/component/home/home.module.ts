import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';
import {HomeRoutes} from './home-routing.module';
import {FormValidatorModule} from 'src/app/shared/form-validator/form-validator.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(HomeRoutes),
    TranslateModule.forChild({}),
    FormValidatorModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    HomeComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class HomeModule {}
