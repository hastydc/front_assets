import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {HttpClientService} from 'src/app/core/service/http-client.service';
import {FormBuilder, Validators} from '@angular/forms';
import {FormValidatorDirective} from 'src/app/shared/form-validator/form-validator.directive';
import {TranslateService} from '@ngx-translate/core';

declare var $;

@Component({
  selector: 'app-home',
  styleUrls: ['./home.component.scss'],
  templateUrl: './home.component.html'
})

export class HomeComponent implements OnInit {
  @ViewChild(FormValidatorDirective, {static: false}) validator;
  assetForm: any;
  active: number;
  title: string;
  dataList: any;
  createOrEdit = 0;
  assetId = 0;
  personAreaList: any;
  assetStateList: any;
  asset: any;
  asset5Form: any;
  type: any;
  serial: any;
  purchaseDate: any;

  constructor(
    private http: HttpClientService,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    private changes: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.build4Form();
  }

  build4Form() {
    this.assetForm = this.formBuilder.group({
      id: '',
      name: ['', Validators.required],
      description: '',
      type: ['', Validators.required],
      serial: ['', Validators.required],
      inventoryNumber: ['', [Validators.required, Validators.pattern('^[0-9]*$'),]],
      weight: ['', Validators.required],
      height: ['', Validators.required],
      width: ['', Validators.required],
      lengthy: ['', Validators.required],
      color: ['', Validators.required],
      assetState: ['0', [Validators.required, Validators.min(1)]],
      purchaseValue: ['', [Validators.required, Validators.pattern('^[0-9]+(.[0-9]{0,2})?$')]],
      purchaseDate: ['', Validators.required],
      dischargeDate: ['', Validators.required],
      assignedTo: ['', Validators.required],
      person: '0',
      area: '0',
      personOrArea: ['0', Validators.min(1)]
    });
  }

  build5Form() {
    const self = this;

    this.asset5Form = this.formBuilder.group({
      type: '',
      serial: '',
      purchaseDate: ''
    });

    this.changes.detectChanges();
    const dateProperties = {
      dateFormat: 'yy-mm-dd',
      onSelect: function(v) {
        self.asset5Form.get('purchaseDate').setValue(v);
      }
    };

    $('#purchaseDate2').datepicker(dateProperties);
  }

  setActive(e: any, step: number) {
    $('.action').removeClass('active');
    $(e.target).addClass('active');
    this.active = step;
    this.title = this.translate.getParsedResult(1, 'common.subTitle.' + this.active);

    if (this.active == 4) {
      this.changes.detectChanges();
      this.build4Form();
    }

    if (this.active == 5) {
      this.dataList = null;
      this.build5Form();
    }
  }

  findAllAssets(): void {
    this.http.httpGet('asset/find-all').subscribe(response => {
      this.dataList = response.data.assetList;
    });
  }

  findAllPersons(): void {
    this.http.httpGet('person/find-all').subscribe(response => {
      this.dataList = response.data.personList;
    });
  }

  findAllAreas(): void {
    this.http.httpGet('area/find-all').subscribe(response => {
      this.dataList = response.data.areaList;
    });
  }

  loadAsset(): void {
    if (this.createOrEdit == 2) {
      this.findAllAssets();
    }

    this.http.httpGet('asset-state/find-all').subscribe(response => {
      this.assetStateList = response.data.assetStateList;
    });

    this.changes.detectChanges();
    this.initDatePicker(['purchaseDate', 'dischargeDate'], this.assetForm);
  }

  getAssetData(): void {
    if (this.assetId) {
      this.http.httpGet('asset/?id=' + this.assetId).subscribe(response => {
        this.asset = response.data.asset;
        this.buildAssetForm(response.data.asset);
        this.changes.detectChanges();
        this.initDatePicker(['purchaseDate', 'dischargeDate'], this.assetForm);
      });
    }
  }

  buildAssetForm(asset) {
    const self = this;
    const ignore = ['purchaseDate', 'dischargeDate', 'assetState', 'area', 'person'];

    Object.keys(asset).map((k) => {
      switch (k) {
        case 'assetState':
          self.assetForm.get(k).setValue(asset[k].id);
          break;

        case 'purchaseDate':
        case 'dischargeDate':
          self.assetForm.get(k).setValue(this.formatDate(asset[k]));
          break;

        case 'person':
          if (asset[k]) {
            self.assetForm.get('personOrArea').setValue(1);
            self.loadPersonOrArea();
            self.assetForm.get(k).setValue(asset[k].id);
          }
          break;

        case 'area':
          if (asset[k]) {
            self.assetForm.get('personOrArea').setValue(2);
            self.loadPersonOrArea();
            self.assetForm.get(k).setValue(asset[k].id);
          }
          break;

        default:
          self.assetForm.get(k).setValue(asset[k]);
          break;
      }
    });
  }

  loadPersonOrArea(): void {
    const personOrArea = this.assetForm.get('personOrArea').value;
    if (this.assetForm.get('personOrArea').value) {
      this.http.httpGet((personOrArea == 1 ? 'person' : 'area') + '/find-all').subscribe(response => {
        this.personAreaList = personOrArea == 1 ? response.data.personList : response.data.areaList;
      });
    }
  }

  createOrEditAsset(): void {
    const personOrArea = this.assetForm.get('personOrArea').value;
    this.assetForm.get('assignedTo').setValue(personOrArea && personOrArea == 1 ? 'person' : 'area');

    if (this.validator.hasErrors().length == 0) {
      this.http.httpPost('asset/create-or-edit', this.assetForm.controls).subscribe(response => {
        this.assetForm.reset();
        this.asset = null;
        alert(response.description);
        location.reload();
      });
    }
  }

  findAssetByField(): void {
    this.dataList = null;
    this.asset5Form.get('type').setValue(this.type);
    this.asset5Form.get('serial').setValue(this.serial);

    this.http.httpPost('asset/find-all-by-field', this.asset5Form.controls).subscribe(response => {
        this.dataList = response.data.assetList;
    });
  }

  initDatePicker(elements, form) {
    this.changes.detectChanges();
    const self = this;
    let dateProperties = {};

    $.datepicker.setDefaults({
      changeMonth: true,
      changeYear: true,
      yearRange: '-100:+100',
      dateFormat: 'yy-mm-dd',
      firstDay: 1,
      showMonthAfterYear: false,
      yearSuffix: ''
    });

    for (const element of elements) {
      if (form.controls[element]) {
        dateProperties = {
          onSelect: function(v) {
            form.controls[$(this).attr('name')].setValue(v);
            self.validator.check(this);
          }
        };
      }

      $('#' + element).datepicker(dateProperties);
    }
  }

  formatDate(d) {
    return new Date(d).toJSON().slice(0, 10).split('-').join('-');
  }

}
