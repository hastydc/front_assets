import { NgModule } from '@angular/core';
import {FormValidatorDirective} from './form-validator.directive';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    TranslateModule.forChild({}),
  ],
  declarations: [FormValidatorDirective],
  exports: [FormValidatorDirective]
})
export class FormValidatorModule { }
