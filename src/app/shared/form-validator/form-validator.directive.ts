import {ChangeDetectorRef, Directive, ElementRef, HostListener, Input, OnChanges, OnInit, Renderer2} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

declare var $;
@Directive({
  selector: '[appFormValidator]',
  exportAs: 'appFormValidator'
})

export class FormValidatorDirective implements OnInit {
  @Input() formGroup: FormGroup;

  constructor(
    private translate: TranslateService,
    private el: ElementRef,
    private changes: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    const self = this;
    this.build();

    $('.submit').click(function() {
      self.checkAll();
    });
  }

  build() {
    const self = this;
    this.changes.detectChanges();

    $(this.el.nativeElement).find('input, select, textarea').each(function() {
      $(this).bind('blur', function() {
        self.check(this);
      });
    });
  }

  check(element, te?) {
    const self = this;
    const name = $(element).attr('name');
    let response = true;

    if (name && name !== 'cb1' && self.formGroup.controls[name] && self.formGroup.controls[name]['errors']) {
      Object.keys(self.formGroup.controls[name].errors).map(function (k) {
        const error = self.translate.getParsedResult(1, 'error.' + k);
        $(element).parent().find('.error').text(error);
        $(element).parent().addClass('error');

        if (te) {
          $('.toast-error').fadeIn();
          setTimeout(function() { $('.toast-error').fadeOut('slow'); }, 2500);
        }
      });
      response = false;
    } else {
      $(element).parent().find('label.error').text('');
      $(element).parent().removeClass('error');
      response = true;
    }

    return response;
  }

  @HostListener('submit')
  checkAll() {
    const self = this;

    $(self.el.nativeElement).find('input, select, checkbox, textarea').each(function() {
      self.check(this, true);
    });
  }

  hasErrors(notShow?) {
    const self = this;
    const errors = [];

    Object.keys(self.formGroup.controls).map(function(k) {
      if (self.formGroup.controls[k].errors) {
        errors.push(k);
      }
    });

    if (errors.length > 0 && !notShow) {
      self.checkAll();
    }

    return errors;
  }
}

