import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

declare var $;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private translate: TranslateService) {
    this.translate.setDefaultLang('en');
  }

  setLanguage(lang) {
    $('.l').removeClass('active');
    $('.' + lang).addClass('active');
    this.translate.use(lang);
  }
}
