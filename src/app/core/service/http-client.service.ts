import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {environment} from 'src/environments/environment';
import {TranslateService} from '@ngx-translate/core';

declare var $;

@Injectable()
export class HttpClientService {
  constructor(
    private http: HttpClient,
    private translate: TranslateService
  ) {
  }

  public url(path: string): string {
    return  environment.apiUrl + path;
  }

  public showErrors(error: any): void {
    $('.fieldset').removeClass('error');
    $('.error').text('');

    if (error.error.data.errors) {
      for (const e of error.error.data.errors) {
        const msg = this.translate.getParsedResult(1, 'error.common');
        $('[name="' + e + '"]').parent().find('.error').text(msg);
        $('[name="' + e + '"]').parent().addClass('error');
        }
    }

    alert(error.error.description);
    return(error['status']);
  }

  public httpGet(path): Observable<any> {
    return this.http.get<Observable<any>>(this.url(path))
      .pipe(
        map( data => {
          return data;
        }),
        catchError( error => {
          this.showErrors(error);
          return throwError('error');
        })
      );
  }

  public httpPost(path, params): Observable<any> {
    // params = JSON.stringify(params);
    const headers = {
      headers: new HttpHeaders({'Content-type': 'application/json'})
    };

    const formatParams  = new Object();
    Object.keys(params).map(function(k) {
      formatParams[k] = params[k]['value'];
    });


    return this.http.post<Observable<any>>(this.url(path), formatParams, headers)
      .pipe(
        map( data => {
          return data;
        }),
        catchError( error => {
          this.showErrors(error);
          return throwError('error');
        })
      );
  }
}




